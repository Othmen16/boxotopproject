//
//  MobileHandler.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation


class MobileHandler {
    
    static func getMovies(_ search:String,_ completion: @escaping (SearchData) -> Void) {
        
        let url = "\(MAIN_URL)&s=\(search)*&type=movie&plot=full&y=2018&page=1"
        
        ApiCalls.getData(url) { result in
            switch result {
            case .fail(let error):
                print("error:",error)
                completion(SearchData())
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let userData = try decoder.decode(SearchData.self, from: data)
                    
                    completion(userData)
                }catch {
                    print("error:",error)
                    completion(SearchData())
                }
            }
        }
    }
    
    
    static func getMovieById(_ id:String, _ completion: @escaping (MovieDetails) -> Void) {
        
        let url = "\(MAIN_URL)&i=\(id)"
        
        ApiCalls.getData(url) { result in
            switch result {
            case .fail(let error):
                print("error:",error)
                completion(MovieDetails())
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let userDetails = try decoder.decode(MovieDetails.self, from: data)
                    
                    completion(userDetails)
                }catch {
                    print("error:",error)
                    completion(MovieDetails())
                }
            }
        }
    }
}
