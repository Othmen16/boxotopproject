//
//  Movie.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation

class SearchData: Codable {
    var search : Array<Movie>?
    var response : String?
    var totalResults : String?
    var error : String?
    
    private enum CodingKeys: String, CodingKey {
        //case createdDate
        case search = "Search"
        case response = "Response"
        case totalResults = "totalResults"
        case error = "Error"
    }
}

class Movie:Codable {
    
    var title : String?
    var year : String?
    var imdbID : String?
    var type : String?
    var poster : String?
    
    private enum CodingKeys: String, CodingKey {
        //case createdDate
        case title = "Title"
        case year = "Year"
        case imdbID = "imdbID"
        case type = "Type"
        case poster = "Poster"
    }
}


class MovieDetails:Codable {
    
    var response : String?
    var title : String?
    var year : String?
    var imdbID : String?
    var type : String?
    var poster : String?
    
    var rated : String?
    var released : String?
    var runtime : String?
    var genre : String?
    var director : String?
    
    var writer : String?
    var actors : String?
    var plot : String?
    var language : String?
    var country : String?
    
    var awards : String?
    var metascore : String?
    var imdbRating : String?
    var imdbVotes : String?
    var dvd : String?
    
    var boxOffice : String?
    var production : String?
    var website : String?
    var ratings : Array<Rating>?
    
    private enum CodingKeys: String, CodingKey {
        //case createdDate
        case response = "Response"
        case title = "Title"
        case year = "Year"
        case imdbID = "imdbID"
        case type = "Type"
        case poster = "Poster"
        
        case rated = "Rated"
        case released = "Released"
        case runtime = "Runtime"
        case genre = "Genre"
        case director = "Director"
        
        case writer = "Writer"
        case actors = "Actors"
        case plot = "Plot"
        case language = "Language"
        case country = "Country"
        
        case awards = "Awards"
        case metascore = "Metascore"
        case imdbRating = "imdbRating"
        case imdbVotes = "imdbVotes"
        case dvd = "DVD"
        
        case boxOffice = "BoxOffice"
        case production = "Production"
        case website = "Website"
        case ratings = "Ratings"
    }
}

class Rating:Codable {
    
    var source : String?
    var value : String?
    
    private enum CodingKeys: String, CodingKey {
        //case createdDate
        case source = "Source"
        case value = "Value"
    }
}
