//
//  MoviesViewController.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import UIKit
import CoreData

class MoviesViewController: UIViewController ,UITextFieldDelegate {
    
    @IBOutlet weak var searchTxtField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var movies = Array<Movie>()
    
    @IBOutlet weak var errorLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MobileHandler.getMovies("the") { searchData in
            DispatchQueue.main.async{
                
                if let movies = searchData.search{
                    self.movies = movies
                    // save to local data
                    for movie in movies{
                        self.save(movie)
                    }
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                }else{
                    
                    self.getlocalMovies()
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                }
            }
        }
        tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
        searchTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        searchMovies(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func searchMovies (_ val:String){
        
        if val.count>2 {
        MobileHandler.getMovies(val) { searchData in
            DispatchQueue.main.async{
                
                if let movies = searchData.search{
                    self.movies = movies
                    // save to local data
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else {
                    self.errorLbl.text = searchData.error ?? ""
                    self.tableView.isHidden = true
                }
            }
        }
        }else {
                self.errorLbl.text = "Too many results, please be more specific"
                self.tableView.isHidden = true
            }
    }
    
    func save(_ movie: Movie) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "MovieData",
                                       in: managedContext)!
        
        let person = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        person.setValue(movie.title, forKeyPath: "title")
        person.setValue(movie.type, forKeyPath: "type")
        person.setValue(movie.poster, forKeyPath: "poster")
        person.setValue(movie.year, forKeyPath: "year")
        person.setValue(movie.imdbID, forKeyPath: "imdbID")
        
        // 4
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    func getlocalMovies(){
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "MovieData")
        
        do {
            let moviesLocal = try managedContext.fetch(fetchRequest)
            
            for movie in moviesLocal{
                let movieToAdd = Movie()
                movieToAdd.title = movie.value(forKey: "title") as? String
                movieToAdd.type = movie.value(forKey: "type") as? String
                movieToAdd.poster = movie.value(forKey: "poster") as? String
                movieToAdd.imdbID = movie.value(forKey: "imdbID") as? String
                movieToAdd.year = movie.value(forKey: "year") as? String
                
                self.movies.append(movieToAdd)
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
}
extension MoviesViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movieCell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        movieCell.setMovie(self.movies[indexPath.row])
        return movieCell
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "detailsMovieViewController") as? DetailsMovieViewController)!
//         let vc = (storyboard.instantiateViewController(withIdentifier: "favoriteMoviesViewController") as? FavoriteMoviesViewController)!
        vc.imdbID = self.movies[indexPath.row].imdbID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
