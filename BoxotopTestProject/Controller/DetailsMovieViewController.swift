//
//  DetailsMovieViewController.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import UIKit
import Kingfisher

class DetailsMovieViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var releaseLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var directorLbl: UILabel!
    @IBOutlet weak var webSiteLbl: UILabel!
    
    @IBOutlet weak var posterImg: UIImageView!
    @IBOutlet var imdbRating: [UIButton]!
    @IBOutlet var rtRating: [UIButton]!
    @IBOutlet var myRating: [UIButton]!
    @IBOutlet weak var reviewTxtView: UITextView!
    @IBOutlet weak var synopsisLbl: UILabel!
    
    @IBOutlet weak var actorsLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var imdbID : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        for btn : UIButton  in imdbRating {
            btn.setImage(UIImage(named: "unselected_star"), for: .normal)
            btn.setImage(UIImage(named: "selected_star"), for: .selected)
        }
        for btn : UIButton  in rtRating {
            btn.setImage(UIImage(named: "unselected_star"), for: .normal)
            btn.setImage(UIImage(named: "selected_star"), for: .selected)
        }
        for btn : UIButton  in myRating {
            btn.setImage(UIImage(named: "unselected_star"), for: .normal)
            btn.setImage(UIImage(named: "selected_star"), for: .selected)
        }
        dataView.isHidden = true
        activityIndicator.startAnimating()
        if let id = self.imdbID {
            
            MobileHandler.getMovieById(id) { movieDetails in
                DispatchQueue.main.async{
                    self.titleLbl.text = movieDetails.title ?? ""
                    self.actorsLbl.text = movieDetails.actors ?? "N/A"
                    self.synopsisLbl.text = movieDetails.plot ?? "N/A"
                    self.genreLbl.text = movieDetails.genre ?? "N/A"
                    self.directorLbl.text = movieDetails.director ?? ""
                    self.webSiteLbl.text = movieDetails.website ?? ""
                    self.releaseLbl.text = "Release date: \(movieDetails.released ?? "N/A")"
                    if let urlImg = movieDetails.poster {
                        let url = URL(string: urlImg)!
                        self.posterImg.kf.indicatorType = .activity
                        self.posterImg.kf.setImage(with: url)
                    }
                    
                    let rating = Double(movieDetails.imdbRating ?? "0.0") ?? 0.0
                    let nbrStars = Int(rating.rounded())
                    let valimdbPerFiveStars = Double(nbrStars / 2)
                    let imdbStars = Int(valimdbPerFiveStars.rounded())
                    
                    for btn in self.imdbRating{
                        btn.isSelected = (btn.tag <= imdbStars)
                    }
                    
                    let crticsRating = Int(movieDetails.metascore ?? "0") ?? 0
                    let valPerFiveStars = Double(crticsRating / 20)
                    let criticsStars = Int(valPerFiveStars.rounded())
                    for btn in self.rtRating{
                        btn.isSelected = (btn.tag <= criticsStars)
                    }
                    
                    self.dataView.isHidden = false
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    @IBAction func starPressed(_ sender: Any) {
        let check = sender as! UIButton
        for btn : UIButton  in myRating {
            if(btn.tag <= check.tag){
                btn.isSelected = true
            }else{
                btn.isSelected = false
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkWebSite(_ sender: Any) {
        guard let url = URL(string: self.webSiteLbl.text ?? "") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
  
    @IBAction func sharePressed(_ sender: Any) {
        let shareContent = String(format: "Name and website of the movie\n%@\n%@", self.titleLbl.text ?? "",self.webSiteLbl.text ?? "")
        let activityViewController = UIActivityViewController(activityItems: [shareContent], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
}

