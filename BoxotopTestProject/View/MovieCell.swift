//
//  MovieCell.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//


import UIKit
import Kingfisher

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var posterImg: RadiusImage!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMovie (_ movie: Movie){
        titleLbl.text = movie.title ?? ""
        yearLbl.text = movie.year ?? ""
        
        if let urlImg = movie.poster {
            let url = URL(string: urlImg)!
            posterImg.kf.indicatorType = .activity
            posterImg.kf.setImage(with: url)
        }
    }
    
    override func prepareForReuse() {
        posterImg.kf.cancelDownloadTask()
    }
}
