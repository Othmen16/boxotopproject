//
//  RadiusView.swift
//  BoxotopTestProject
//
//  Created by nino on 09/10/2018.
//  Copyright © 2018 Othmen. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RadiusView: UIView {
    
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
}
